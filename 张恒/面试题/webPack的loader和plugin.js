// loader: 用于对模块资源的转码，
//         loader描述了webpack如何处理非js模块，并且在build中引入这些依赖。
//         loader可以将文件从不同的语言（如ts）转换为js，css-loader等。

// plugin: 目的在于解决loader无法实现的其他事，从打包优化和压缩，到重新定义环境变量，功能强大到可以用来处理各种各样的任务。
//         webpack提供了很多开箱即用的插件

// 主要区别:loader用于加载某些资源文件。因为webpack本身只能打包commonjs规范的js文件，
//         对于其他资源例如css，图片，或者其他的语法集，比如jsx是无法加载的
//         这时候就需要用loader将资源转化，加载进来。
//         plugin用于扩展webpack的功能。他直接御用webpack，扩展了他的功能。当然loader也变相的扩展了webpack的功能
//         但是他只专注于 转化文件（transform）这个领域。而plugin的功能更加的丰富，而不仅局限于资源的加载