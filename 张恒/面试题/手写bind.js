Function.prototype.myBind = function (thisArg) {
    if (typeof this !== 'function') {
        return;
    }
    var _self = this;
    var args = Array.prototype.slice.call(arguments, 1);
    var fnTop = function () {}
    var fnBound = function () {
        // 检测 New
        // 如果当前函数的this指向的是构造函数中的this 则判定为new 操作
        var _this = this instanceof _self ? this : thisArg
        return _self.apply(_this, args.concat(Array.prototype.slice.call(arguments)))

    }
    // 为了完成 new操作
    // 还需要做一件事情 执行原型 链接 （思考题，为什么？
    if (this.prototype) {
        fnTop.prototype = this.prototype;
    }
    fnBound.prototype = new fnNop();

    return fnBound;
}


Function.prototype.binds = function (thisArg) {
    if (typeof this !== 'function') return
    let args = Array.prototype.slice.call(arguments, 1);
    let _self = this
    var newFuc = function () {}
    var myFunc = function () {
        let _this = this instanceof _self ? this : thisArg;
        return _self.apply(_this, args.concat(Array.prototype.slice(arguments)));
    }
    if (this.prototype) newFuc.prototype = this.prototype
    myFunc.prototype = new newFuc()
    return myFunc
}
let a = {x:1, y:2}
let b = {
    x: 3,
    dd: function (f) {
        console.log(this.x, this.y, 9)
    }
}

let c = b.dd.binds(a)
c(9)





Function.prototype.myBind  = function (arg) {
    if (typeof this !== 'function') return 
    let _self = this
    let args = Array.prototype.slice(arguments, 1);
    let confuc = function () {}
    let myFunc = function () {
        let newThis = this instanceof _self ? this : arg
        return _self.apply(newThis, args.concat(Array.prototype.slice(arguments)));
    }
    if (this.prototype) confuc.prototype = this.prototype
    myFunc.prototype = new confuc()
    return myFunc
}