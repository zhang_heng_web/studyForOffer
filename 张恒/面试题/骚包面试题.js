
// 首先： 
// 1，map会返回和原数组长度一致的结果，filter则会过滤
// 2，parseInt 有两个参数（string, redix） redix的参数如果不传或者为0的时候会以string的值来做默认基数
// 3，parseFloat 只有一个参数 只需要看第一个参数是否能正常转换为数字就行。
[1,2,3].map(parseInt)
// [1,NaN,NaN]     
// 其实在使用map时，map的callback的第二个参数index引索值就成为parseeInt的radix值。
// ['1', '2', '3'].map(parseInt)在遍历的过程。其实是经历了下面的过程
// parseInt('1', 0)：radix的值为0，判断字符串发现介于1~9，用10进制转换，结果为1.
// parseInt('2', 1)：radix的值为1，如果该参数小于 2 或者大于 36，则 parseInt() 将返回 NaN。
// parseInt('3', 2): radix的值为2，这就意味着字符串将被解析成字节数，也就是仅仅包含数值0和1。parseInt的规范指出，它仅尝试分析第一个字符的左侧。这个字符串的第一个字符是“3”，它并不是基础基数2的一个有效数字。所以这个子字符串将被解析为空。如果子字符串被解析成空了，函数将返回为NaN。
[1,2,3].map(parseFloat)
// [1,2,3]
[1,2,3].filter(parseInt)
// [1]
// filter在内部则会认为NaN为false，故不返回

[1,2,3].filter(parseFloat)
// [1,2,3]

'1 2 3'.replace(/\d/g, parseInt)
// "1 NaN 3"

['1', '2', '3'].reduce(parseInt)
// 1
