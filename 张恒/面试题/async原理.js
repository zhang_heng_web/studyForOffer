// async 是promise的语法糖

// 通过promise 和 generator函数实现

function readFile(a) {
    return new Promise(resolve => {
        setTimeout(()=> {
            console.log(a)
            resolve(a)
        }, 500)
    })
}
function *foo () {
    console.log('a')
    var result = yield readFile('b')
    console.log('c')
}
function run(g) {
    var res = g.next()
    if(!res.done) {
        res.value.then(() => {
            run(g)
        })
    }
}

run(foo())

