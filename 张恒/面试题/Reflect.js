// 1、什么是Reflect？
// 为操作对象而提供的新API 确保对象的属性能正确赋值，广义上讲，即确保对象的原生行为能够正常进行，这就是Reflect的作用

// 2、为什么要设计Reflect？
// （1）将Object对象的属于语言内部的方法放到Reflect对象上，即从Reflect对象上拿Object对象内部方法。
// （2）将用 老Object方法 报错的情况，改为返回false
// 老写法
try {
  Object.defineProperty(target, property, attributes);
  // success
} catch (e) {
  // failure
}

// 新写法
if (Reflect.defineProperty(target, property, attributes)) {
  // success
} else {
  // failure
}

// （3）让Object操作变成函数行为
// 老写法（命令式写法）

'name' in Object //true

// 新写法

Reflect.has(Object,'name') //true

// （4）Reflect与Proxy是相辅相成的，在Proxy上有的方法，在Reflect就一定有

let target={}
    let handler={
      set(target,proName,proValue,receiver){
        //确认对象的属性赋值成功
        let isSuccess=Reflect.set(target,proName,proValue,receiver)
        if(isSuccess){
          console.log("成功")
        }
        return isSuccess
      }
    }
    let proxy=new Proxy(target,handler)
