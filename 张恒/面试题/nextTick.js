// nextTick 原理：
// 优雅降级：
//     Promise.then()
//     setImmediate
//     一个浏览器目前支持ubutaihao的api
//     setTimeout

// 由于dom真正更新渲染好的时间，不能真正的确定，
// 不论是框架还是原生，都存在这个问题。所以nextTick并不能完全保证拿到最新的dom