// 用构造函数创建对象，new做了4步

// 1.创建一个新的空对象                              var obj = {};
// 2.新对象的__proto__指向构造函数的原型对象          obj.__proto__ = 构造函数.prototype
// 3.构造函数的this指向正在创建的新对象               构造函数.call(obj);
//    执行构造函数的代码，向新对象中添加属性和方法
// 4.返回新对象地址