let s1 = Symbol()
let s2 = Symbol('another symbol')
typeof s1  // 'symbol'
let s1 = Symbol()
let s2 = Symbol('another symbol')
let s3 = Symbol('another symbol')

s1 === s2 // false
s2 === s3 // false


// Symbol可同样用于对象属性的定义和访问：
const PROP_NAME = Symbol()
const PROP_AGE = Symbol()

let obj = {
  [PROP_NAME]: "一斤代码"
}
obj[PROP_AGE] = 18

obj[PROP_NAME] // '一斤代码'
obj[PROP_AGE] // 18


let obj = {
    [Symbol('name')]: '一斤代码',
    age: 18,
    title: 'Engineer'
 }
 
 Object.keys(obj)   // ['age', 'title']
 
 for (let p in obj) {
    console.log(p)   // 分别会输出：'age' 和 'title'
 }
 
 Object.getOwnPropertyNames(obj)   // ['age', 'title']
 JSON.stringify(obj)  // {"age":18,"title":"Engineer"}

 // 使用Object的API

 Object.getOwnPropertySymbols(obj) // [Symbol(name)]
 // 使用新增的反射API
Reflect.ownKeys(obj) // [Symbol(name), 'age', 'title']

// 由上代码可知，Symbol类型的key是不能通过Object.keys()或者for...in来枚举的，
// 它未被包含在对象自身的属性名集合(property names)之中。
// 所以，利用该特性，我们可以把一些不需要对外操作和访问的属性使用Symbol来定义。